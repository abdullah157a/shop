@extends('admin.layouts.master')
@section('title','View Product')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="header-icon">
          <i class="fa fa-product-hunt"></i>
       </div>
       <div class="header-title">
          <h1>View Products</h1>
          <small>Products Lst</small>
       </div>
    </section>
    <!-- Main content -->
    <section class="content">
       <div class="row">
          <div class="col-sm-12">
             <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                   <div class="btn-group" id="buttonexport">
                      <a href="#">
                         <h4>View Products</h4>
                      </a>
                   </div>
                </div>
                <div class="panel-body">
                <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                   <div class="btn-group">
                      <div class="buttonexport" id="buttonlist"> 
                         <a class="btn btn-add" href="{{url('admin/add-product')}}"> <i class="fa fa-plus"></i> Add Product
                         </a>  
                      </div>
                   </div>
                   <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                   <div class="table-responsive">
                      <table id="table_id" class="table table-bordered table-striped table-hover">
                         <thead>
                            <tr class="info">
                                
                               <th>Product Name</th>
                               <th>Product Code</th>
                               <th>Product Color</th>
                               <th>Product description</th>
                               <th>Image</th>
                               <th>Price</th>
                               <th>Status</th>
                               <th>Action</th>
                            </tr>
                         </thead>
                         <tbody>
                             @foreach ($products as $item)
                             <tr>
                                <td>{{$item->name}}</td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->color}}</td>
                                <td>{{$item->description}}</td>
                                
                                <td>
                                    @if(!empty($item->image))
                                    <img src="{{asset('/uploads/products/'.$item->image)}}" alt="" style="width: 100px;">
                                </td>
                                @endif
                                <td>{{$item->price}}</td>                         
                                <td><span class="label-custom label label-default">Active</span></td>
                                <td>
                                   {{-- <a href="{{url('/admin/edit-product'.$item->id)}}" class="btn btn-add btn-sm"><i class="fa fa-pencil"></i></a> --}}
                                   <a href="{{url('admin/delete-product/'.$item->id)}}" class="btn btn-danger btn-sm" ><i class="fa fa-trash-o"></i> </a>
                                   <a  href="{{url('admin/edit-product'.$item->id)}}" class="btn btn-add btn-sm"> 
                                    <i class="fa fa-pencil"></i></a>  
                                </td>
                             </tr>
                             @endforeach
                          
                            
                         </tbody>
                      </table>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <!-- customer Modal1 -->
     
       <!-- /.modal -->
    </section>
    <!-- /.content -->
 </div> 
@endsection
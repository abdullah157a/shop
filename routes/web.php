<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], 'wayshop', [IndexController::class, 'index']);
Route::match(['get', 'post'], '/admin', [AdminController::class, 'login']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => ['auth']], function () {
    Route::match(['get', 'post'], '/admin/dashboard', [AdminController::class, 'dashboard']);

    // Category Route
    Route::match(['get', 'post'], '/admin/add-category', [CategoryController::class, 'addCategory']);

    // Product Route

    Route::match(['get', 'post'], '/admin/add-product', [ProductsController::class, 'addProduct']);
    Route::match(['get', 'post'], '/admin/view-product', [ProductsController::class, 'viewProducts']);
    Route::match(['get', 'post'], '/admin/edit-product{id}', [ProductsController::class, 'editProducts']);
    Route::match(['get', 'post'], '/admin/delete-product/{id}', [ProductsController::class, 'deleteProducts']);
});

Route::get('/logout', 'AdminController@logout');

<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Products;


class ProductsController extends Controller
{
    public function addProduct(Request $request)
    {
        if ($request->ismethod('post')) {
            $data = $request->all();
            // echo "<pre>";
            // print_r($data);
            // die;
            $product = new Products;
            $product->category_id = $data['category_id'];
            $product->name = $data['product_name'];
            $product->code = $data['product_code'];
            $product->color = $data['product_color'];
            if (!empty($data['product_description'])) {
                $product->description = $data['product_description'];
            } else {
                $product->description = '';
            }

            $product->price = $data['product_price'];

            if ($request->has('image')) {
                $file = $request->image;
                $filename = $file->getClientOriginalName();
                $product->image = $filename;
                $file->move('uploads/products/', $filename);
            }
            $product->save();
            return redirect('/admin/add-product')->with('flash_message_success', 'Product has been added successfully');
        }
        // Categories Drop Down menu Code

        $categories = Category::where(['parent_id' => 0])->get();
        $categories_dropdown = "<select name='category_id' id='category_id' class='form-control' >";
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='" . $cat->id . "'>" . $cat->name . "</option>";
            $sub_categories = Category::where(['parent_id' => $cat->id])->get();
            foreach ($sub_categories as $sub_cat) {
                $categories_dropdown .= "<option value='" . $sub_cat->id . "'>" . $sub_cat->name . "</option>";
            }
        }
        $categories_dropdown .= "</select>";
        return view('admin.products.add_product')->with(compact('categories_dropdown'));
    }


    public function viewProducts()
    {
        $products = Products::get();
        return view('admin.products.view_products')->with(compact('products'));
    }

    public function editProducts(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            if ($request->has('image')) {
                $file = $request->image;
                $filename = $file->getClientOriginalName();

                $file->move('uploads/products/', $filename);
            } else {
                $filename = $data['current_image'];
            }
            if (empty($data['product_description'])) {
                $data['product_description'] = '';
            }
            Products::where(['id' => $id])->update(['name' => $data['product_name'], 'code' => $data['product_code'], 'color' => $data['product_color'], 'description' => $data['product_description'], 'price' => $data['product_price'], 'image' => $filename]);
            return redirect()->back()->with('flash_message_success', 'Product has been Updated!');
        }

        $productDetails = Products::where(['id' => $id])->first();
        return view('admin.products.edit_product')->with(compact('productDetails'));
    }

    public function deleteProducts($id = null)
    {
        Products::where(['id' => $id])->delete();
        return redirect()->back()->with('flash_message_error', 'Data Deleted Successfully');
    }
}
